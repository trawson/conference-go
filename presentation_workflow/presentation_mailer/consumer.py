import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

while True:
    try:
        parameters = pika.ConnectionParameters(host='rabbitmq')
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()

        def process_approval(ch, method, properties, body):
            presentation = json.loads(body)

            send_mail(
                'Your presentation has been accepted. So much love.',
                message=f"{presentation['name']}, We Love You. You Are Approved. You may share your presentation {presentation['title']}. Be Welcome.",
                from_email='foo@bar.com',
                recipient_list=[presentation['presenter_email']],
                fail_silently=False,
            )
            channel.queue_declare(queue='presentation_approvals')
            channel.basic_consume(
                queue='presentation_approvals',
                on_message_callback=process_approval,
                auto_ack=True,
            )
            channel.start_consuming()

        def process_rejection(ch, method, properties, body):
            presentation = json.loads(body)
            send_mail(
                'Your presentation has been rejected',
                message=f"{presentation['name']} We Don/t Like You. You Are Rejected. You may not share your presentation {presentation['title']}. Be Rejected.",
                from_email='foo@bar.com',
                recipient_list=[presentation['presenter_email']],
                fail_silently=False,
            )
            channel.queue_declare(queue='presentation_approvals')
            channel.basic_consume(
                queue='presentation_approvals',
                on_message_callback=process_approval,
                auto_ack=True,
            )
            channel.start_consuming()

    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
